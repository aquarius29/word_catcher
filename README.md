This is a small project to help a friend learn English but it can be used with any language. 

The goal was to collect a few favorite movies of a person along with their subtitles. 
The person can specify a word (or phrase) to search in the subtitles and then the script will cut out these scenes and generate a short video which will contains the scenes where the search word is spoken. 

installation 
run python3.6 setup.py install

this will install pysrt and moviepy

how to use: 
python word_catcher 'the search word/phrase'

Output of the script - a video file will be placed in the *output/* folder in the same location as the script. 

All the input files (movies and their subs) should be located in the *input/* folder.
All the movies should have naming format: 
movie_name.mp4
The subtitle file should have the same name as the movie file but with .srt ending:
movie_name.srt


Possible implementations:

1. Telegram bot which will spit out a small video clip with the searched word

2. Add some features like maximum length of the clip (should not be longer than 20 seconds)
2.1 Maybe possibility to specify movie by name of movie set by genre.

3. see into allowing multiple subtitle format
4. search the phrases that span multiple lines in the subs file. Maybe do the replace of new line in the file before searching. Subtiltles can be split into 2 lines and phrase migh be on different lines.


SPLITTER 
is a subservice, to just extract a clip from the movie. 
usage: 
python splitter.py filename start_time end_time
ex: 
python splitter.py moviename 00:10:32 00:12:03