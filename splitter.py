import sys
from os import listdir
import os
from moviepy.editor import VideoFileClip as vfc 

def crate_subclip(video, start, end):
    videoclip = vfc(video)
    return videoclip.subclip(str(start), str(end))
    
name = sys.argv[1]
start = sys.argv[2]
end = sys.argv[3]

output_folder = "output/"
input_folder = "input/"
files = listdir(input_folder)

video_file = input_folder + name 

if not video_file.find('.mp4')>0:
    video_file += ".mp4"

print(" splitting video : " + video_file)
movie_clip = crate_subclip(os.path.abspath(video_file), start, end)
movie_clip.write_videofile(output_folder + name + "_subclip" + ".mp4")

